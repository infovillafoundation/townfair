package org.bitbucket.infovillafoundation.wayyan.townfair;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 3/12/2014.
 */

@FXMLController(value = "/fxml/game.fxml", title="New Game")
public class GameController {
    @Inject
    private TownFair townFair;

    @FXML
    private TextField newGameName;

    @FXML
    private TextField newGameBoothNo;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("createdLevel")
    private Button created;

    @ActionMethod("createdLevel")
    public void createdLevel() {
        String GameName = newGameName.getText();
        int GameBoothNo = Integer.parseInt(newGameBoothNo.getText());
        Game game = new Game(GameName, GameBoothNo);
        townFair.getGames().add(game);
    }
}
