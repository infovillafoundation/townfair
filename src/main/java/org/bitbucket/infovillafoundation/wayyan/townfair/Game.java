package org.bitbucket.infovillafoundation.wayyan.townfair;

/**
 * Created by Admin on 3/12/2014.
 */
public class Game {
    String name;
    int boothNumber;

    public Game(String name, int boothNumber) {
        this.name = name;
        this.boothNumber = boothNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBoothNumber() {
        return boothNumber;
    }

    public void setBoothNumber(int boothNumber) {
        this.boothNumber = boothNumber;
    }

    @Override
    public String toString() {
        return "Game{" +
                "name='" + name + '\'' +
                ", boothNumber=" + boothNumber +
                '}';
    }
}
