package org.bitbucket.infovillafoundation.wayyan.townfair;

import org.datafx.controller.injection.ApplicationScoped;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/12/2014.
 */

@ApplicationScoped
public class TownFair {
    List<Child> children = new ArrayList<>();
    List<Game> games = new ArrayList<>();

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public List<String> getGameNames() {
        List<String> gameNames = new ArrayList<>();
        for (Game game : games) {
            gameNames.add(game.getName());
        }
        return gameNames;
    }

    public Game getGame(String name) {
        int index = getGameNames().indexOf(name);
        return games.get(index);
    }
}
