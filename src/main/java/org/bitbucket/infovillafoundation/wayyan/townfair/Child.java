package org.bitbucket.infovillafoundation.wayyan.townfair;

/**
 * Created by Admin on 3/12/2014.
 */
public class Child {
    int age;
    String name;
    String gender;
    Game chosenGame;

    public Child(int age, String name, String gender, Game chosenGame) {
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.chosenGame = chosenGame;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Game getChosenGame() {
        return chosenGame;
    }

    public void setChosenGame(Game chosenGame) {
        this.chosenGame = chosenGame;
    }

    @Override
    public String toString() {
        return "Child{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", chosenGame='" + chosenGame + '\'' +
                '}';
    }
}
