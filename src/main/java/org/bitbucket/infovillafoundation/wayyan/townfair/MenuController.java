package org.bitbucket.infovillafoundation.wayyan.townfair;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.LinkAction;

/**
 * Created by Admin on 3/12/2014.
 */

@FXMLController(value = "/fxml/menu.fxml", title="Menu")
public class MenuController {
    @FXML
    @LinkAction(ChildController.class)
    private Button childRegister;

    @FXML
    @LinkAction(GameController.class)
    private Button createGames;
}
