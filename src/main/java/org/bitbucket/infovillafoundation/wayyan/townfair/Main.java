package org.bitbucket.infovillafoundation.wayyan.townfair;

import javafx.application.Application;
import javafx.stage.Stage;
import org.datafx.controller.flow.Flow;

/**
 * Created by Admin on 3/12/2014.
 */

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        new Flow(MenuController.class).startInStage(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
