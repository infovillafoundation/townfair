package org.bitbucket.infovillafoundation.wayyan.townfair;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.xml.soap.Text;

/**
 * Created by Admin on 3/12/2014.
 */

@FXMLController(value = "/fxml/child.fxml", title="Child Details")
public class ChildController {
    @Inject
    private TownFair townfair;

    @FXML
    private TextField childName;

    @FXML
    private TextField childAge;

    @FXML
    private ComboBox<String> childGender;

    @FXML
    private ComboBox<String> childGames;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("createdChild")
    private Button booked;

    @PostConstruct
    public void init() {
        childGames.getItems().setAll(townfair.getGameNames());
    }

    @ActionMethod("createdChild")
    public void createdChild() {
        String name = childName.getText();
        int age = Integer.parseInt(childAge.getText());
        String gender = childGender.getValue();
        String gameName = childGames.getValue();
        Game game = townfair.getGame(gameName);
        Child child = new Child(age, name, gender, game);
        townfair.getChildren().add(child);
    }

}
